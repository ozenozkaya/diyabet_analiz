# Diyabet Analizörü

Diyabet analizörü, libre vb sensörlerden gelen verileri yorumlayacak, kullanıcı ve doktorlara yardımcı bir Java Swing (masaüstü) uygulamasıdır. 

## Başlamak için

Diyabet analizörü IntelliJ IDE üzerinde geliştirilmiş Java tabanlı bir masaüstü uygulamasıdır. Uygulamayı kullanmaya başlamak için bilgisayarınızda Java Runtime Environment yüklü olmalıdır. Uygulamayı geliştirmeye katkıda bulunmak istiyorsanız Java Developer Kit yüklü olmalıdır. Geliştirme ortamı olarak IntelliJ IDE önerilmekle birlikte, istenen herhangi bir arayüz kullanılabilir.

### Prerequisites

JRE: http://www.oracle.com/technetwork/java/javase/downloads/jre9-downloads-3848532.html

JDK: http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html

IntelliJ Community Edition: https://www.jetbrains.com/idea/download/#section=windows

### Kurulum

Uygulama için bir kurulum dosyası oluşturulacaktır.


## Geliştirme ortamı

* [IntelliJ Community Edition](https://www.jetbrains.com/idea/download/#section=windows) - JAVA IDE
* [Maven](https://maven.apache.org/) - Bağımlılık (dependency) yönetimi

## Katkıda Bulunma

Katkıda bulunmak isterseniz ozenozkaya[at]gmail.com mail adresi üzerinden iletişime geçerek projeye katılabilirsiniz.

## Geliştirici Listesi

* **Doç. Dr. Berna Örs Yalçın** - *Danışmanlık, planlama, yönetim ve gerçekleme* 
* **Özen Özkaya** - *Mimari, tasarım ve gerçekleme* 

## Lisans

Bu proje MIT License ile lisanslanmıştır.

## Teşekkürler
Sn. Prof.Dr. Şükrü Hatun
Sn. Doç. Dr. Gül Yeşiltepe Mutlu
