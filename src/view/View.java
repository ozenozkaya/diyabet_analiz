package view;

import model.DataTableModel;
import control.Controller;
import model.StatCalculationModel;
import org.jdatepicker.JDatePanel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;


public class View {
    public JFrame frame;
    public JTable table;
    public JTextField libreFilenameTextField;
    private Controller controller;
    private Component GraphTabbedPane;
    private Component StatsTabbedPane;
    private JFreeChart glucose_chart;
    private JPanel stats_panel;
    public View() {


        // Create table model
        DataTableModel model = new DataTableModel();
        //table.setModel(model);

        // Create controller
        this.controller = new Controller(this, model);



        JTabbedPane tabbedPane=new JTabbedPane();
        tabbedPane.add("Veriler", CreateDataTableTabbedPane() );


        GraphTabbedPane = tabbedPane.add("Grafikler", CreateGraphTabbedPane() );


        StatsTabbedPane = tabbedPane.add("İstatistikler", CreateStatisticsPane() );


        // Display it all in a scrolling window and make the window appear
        frame = new JFrame("Diyabet Uygulaması");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(tabbedPane);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private JSplitPane CreateDataTableTabbedPane()
    {
        // Create views swing UI components
        libreFilenameTextField = new JTextField(26);
        libreFilenameTextField.setToolTipText("Lütfen Libre Dosyası Seçiniz");
        JButton filterButton = new JButton("Libre Kaydı Dosyası Aç");
        table = new JTable();
        filterButton.addActionListener(controller);

        // Set the view layout
        JPanel ctrlPane = new JPanel();
        ctrlPane.add(libreFilenameTextField);
        ctrlPane.add(filterButton);

        JScrollPane tableScrollPane = new JScrollPane(table);
        tableScrollPane.setPreferredSize(new Dimension(700, 182));
        tableScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Libre Ölçümü",
                TitledBorder.CENTER, TitledBorder.TOP));

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, ctrlPane, tableScrollPane);
        splitPane.setDividerLocation(35);
        splitPane.setEnabled(false);
        return splitPane;
    }

    private XYDataset createDataset() {

        TimeSeries series = new TimeSeries("Glukoz");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        sdf.setLenient(false);
        for(int i=0;i<this.table.getModel().getRowCount();i++)
        {
            int type = Integer.parseInt(this.table.getModel().getValueAt(i,2).toString());
            if(type == 0){
                Date date = null;
                try {
                    date = sdf.parse(this.table.getModel().getValueAt(i,1).toString());
                    //System.out.println(date);
                } catch(ParseException ex){
                    ex.printStackTrace();
                }

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int index = i;//Integer.parseInt(this.table.getModel().getValueAt(i,0).toString());
                //String datetime = this.table.getModel().getValueAt(i,1).toString();
                int glu = Integer.parseInt(this.table.getModel().getValueAt(i,3).toString());
                series.addOrUpdate( new Minute(cal.get(Calendar.MINUTE),cal.get(Calendar.HOUR),cal.get(Calendar.DAY_OF_MONTH),cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR)),glu);
            }

        }
        return new TimeSeriesCollection(series);
    }

    public void UpdateGraphTabbedPane()
    {
        XYPlot plot = (XYPlot) glucose_chart.getPlot();
        DateAxis dateAxis = new DateAxis();
        dateAxis.setDateFormatOverride(new SimpleDateFormat("yyyy/MM/dd HH:mm"));
        plot.setDomainAxis(dateAxis);
        plot.setDataset(createDataset());
        GraphTabbedPane.repaint();
    }

    public void UpdateStatisticsPane()
    {
        stats_panel.setLayout(new SpringLayout());


        StatCalculationModel calculationModel = new StatCalculationModel(this.table.getModel());
        calculationModel.CalculateStats();

        stats_panel.add(new JLabel("-:    Toplam Örnek:"));
        stats_panel.add(new JLabel(Integer.toString(calculationModel.num_samples)));
        stats_panel.add(new JLabel("-:    Geçerli Örnek:"));
        stats_panel.add(new JLabel(Integer.toString(calculationModel.num_valid_samples)));
        stats_panel.add(new JLabel("1:    Mean:"));
        stats_panel.add(new JLabel(Double.toString(calculationModel.mean)));
        stats_panel.add(new JLabel("2:    Median:"));
        stats_panel.add(new JLabel(Double.toString(calculationModel.median)));
        stats_panel.add(new JLabel("3:    Standard Deviation:"));
        stats_panel.add(new JLabel(Double.toString(calculationModel.std_dev)));
        stats_panel.add(new JLabel("4:    Glycemic varibality:"));
        stats_panel.add(new JLabel(Double.toString(calculationModel.glyceemic_variability)));

        //Lay out the panel.
        SpringUtilities.makeCompactGrid(stats_panel,
                6, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad



        StatsTabbedPane.repaint();
    }

    public void ParseTableModelFromFileInput(DefaultTableModel tableModel)
    {
        JFileChooser fileChooser = new JFileChooser();
        int i = fileChooser.showOpenDialog(this.frame);
        if (i == JFileChooser.APPROVE_OPTION) {
            File recordFile = fileChooser.getSelectedFile();
            String filePath = recordFile.getPath();
            this.libreFilenameTextField.setText(filePath);
            try {
                BufferedReader br = new BufferedReader(new FileReader(filePath));
                String line = "";
                //ArrayList<Object[]> records = new ArrayList<Object[]>();
                //DefaultTableModel tableModel = new DefaultTableModel(Constants.TABLE_HEADER, 0);
                while ((line = br.readLine()) != null) {
                    if(Character.isDigit(line.charAt(0))){
                        String[] params = line.split("\t", -1);
                        tableModel.addRow(params);
                        //records.add(params);
                    }
                }

                JOptionPane.showMessageDialog(null, "Dosya okundu");
                this.libreFilenameTextField.setToolTipText("Okunan Dosya");

                this.table.setModel(tableModel);
                this.table.setAutoCreateRowSorter(true);
                this.UpdateGraphTabbedPane();
                this.UpdateStatisticsPane();


                br.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

    }

    private JPanel CreateStatisticsPane()
    {
        stats_panel = new JPanel();

        return stats_panel;
    }

    private JPanel CreateGraphTabbedPane()
    {
        JPanel dataGraphPanel = new JPanel();
        dataGraphPanel.setLayout((new GridLayout(1,2)));


        TimeSeriesCollection dataset = new TimeSeriesCollection();
        //dataset.setDomainIsPointsInTime(true);


        glucose_chart = ChartFactory.createTimeSeriesChart(
                "Glukoz Grafiği",  // title
                "Zaman",             // x-axis label
                "Glukoz Değeri (mg/dL)",   // y-axis label
                createDataset(),            // data
                true,               // create legend?
                true,               // generate tooltips?
                false               // generate URLs?
        );
        glucose_chart.setBackgroundPaint(Color.white);



        XYPlot plot = (XYPlot) glucose_chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);



        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(false);
            renderer.setBaseShapesFilled(false);
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("YYYY/MM/DD HH:mm"));

        ChartPanel chartPanel = new ChartPanel(glucose_chart);
        dataGraphPanel.add(chartPanel);

        JPanel graphControlPanel = new JPanel();
        graphControlPanel.setLayout(new SpringLayout());
        graphControlPanel.add(new JLabel("Ölçüm Tipi:"));
        String meas_type_list[] = {"0", "1", "2", "3","4","5","Tümü"};
        graphControlPanel.add(new JComboBox(meas_type_list));
        //graphControlPanel.add(new JButton("Grafiği Çiz"));
        graphControlPanel.add(new JLabel("Başlangıç Tarihi:"));


        UtilDateModel model_start = new UtilDateModel();
        UtilDateModel model_end = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Bugün");
        p.put("text.month", "Ay");
        p.put("text.year", "Yıl");

        graphControlPanel.add(new JDatePanelImpl(model_start, p));
        graphControlPanel.add(new JLabel("Bitiş Tarihi:"));
        graphControlPanel.add(new JDatePanelImpl(model_end, p));
        graphControlPanel.add(new JLabel(""));
        graphControlPanel.add(new JButton("Grafiği güncelle"));


        //Lay out the panel.
        SpringUtilities.makeCompactGrid(graphControlPanel,
                4, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        dataGraphPanel.add(graphControlPanel);
        dataGraphPanel.setVisible(true);

        return dataGraphPanel;
    }

}