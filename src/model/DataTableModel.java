package model;

import javax.swing.table.DefaultTableModel;

/**
 * @author ozenozkaya
 *
 */
@SuppressWarnings("serial")
public class DataTableModel extends DefaultTableModel {

    public DataTableModel() {
        super(Constants.DATA, Constants.TABLE_HEADER);
    }
}
