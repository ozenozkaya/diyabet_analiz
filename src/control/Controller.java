package control;

import model.Constants;
import view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Objects;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * @author ashraf
 */
public class Controller implements ActionListener {

    private JTextField searchTermTextField = new JTextField(26);
    private DefaultTableModel model;
    private View view;

    public Controller(View view, DefaultTableModel model) {
        super();
        this.view = view;
        this.model = model;
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        DefaultTableModel tableModel = new DefaultTableModel(Constants.TABLE_HEADER, 0);
        this.view.ParseTableModelFromFileInput(tableModel);
        this.model = tableModel;
    }

}