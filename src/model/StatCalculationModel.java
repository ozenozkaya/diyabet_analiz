package model;


import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.util.Arrays;

public class StatCalculationModel {
    public int num_samples;
    public int num_valid_samples;
    public double mean;
    public double median;
    public double std_dev;
    public double glyceemic_variability;

    public TableModel tableModel;
    public StatCalculationModel(TableModel model)
    {
        this.tableModel = model;
        num_samples=0;
        num_valid_samples=0;
        mean=0.0;
        median=0.0;
        std_dev=0.0;
        glyceemic_variability=0.0;
    }

    public void CalculateStats()
    {

        num_samples = this.tableModel.getRowCount();

        int[] numArray = new int[num_samples];
        mean = 0;
        num_valid_samples=0;
        for(int i=0; i< num_samples; i++)
        {
            int glu = 0;
            try{
                glu = Integer.parseInt(this.tableModel.getValueAt(i,3).toString());
            }catch(NumberFormatException ex){ // handle your exception
                glu=0;
            }
            //int glu = Integer.parseInt(this.table.getModel().getValueAt(i,3).toString());
            if(glu != 0)
            {
                mean += glu;
                numArray[num_valid_samples] = glu;
                num_valid_samples++;
            }

        }
        mean /= num_valid_samples;

        double[] stdDevArray = new double[num_valid_samples];
        std_dev = 0.0;
        for(int i = 0; i < num_valid_samples; i++){
            stdDevArray[i] = Math.pow(( numArray[i]- mean),2);
            std_dev+=stdDevArray[i];
        }
        std_dev /=num_valid_samples;
        std_dev = Math.sqrt(std_dev);


        Arrays.sort(numArray);
        median=0.0;
        if (numArray.length % 2 == 0)
            median = ((double)numArray[numArray.length/2] + (double)numArray[numArray.length/2 - 1])/2;
        else
            median = (double) numArray[numArray.length/2];


        glyceemic_variability = std_dev / mean;

    }



}
