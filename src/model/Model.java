package model;

import javax.swing.table.DefaultTableModel;

/**
 * @author ozenozkaya
 *
 */
@SuppressWarnings("serial")
public class Model extends DefaultTableModel {

    public Model() {
        super(Constants.DATA, Constants.TABLE_HEADER);
    }
}